<?php
use Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    "Avanti_OrderSellerEmail",
    __DIR__
);
