<?php
namespace Avanti\OrderSellerEmail\Plugin;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class SellerEmail
{
    /**
     * Order Extension Attributes Factory
     *
     * @var OrderExtensionFactory
     */
    protected $orderExtensionFactory;

    private $authSession;

    /**
     * SellerEmail constructor.
     * @param OrderExtensionFactory $orderExtensionFactory
     * @param AuthSession $authSession
     */
    public function __construct(
        OrderExtensionFactory $orderExtensionFactory,
        AuthSession $authSession)
    {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->authSession = $authSession;
    }

    /**
     * Insert seller email in order Get API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $sellerEmail = $order->getSellerEmail();

        if($sellerEmail != null) {
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setSellerEmail($sellerEmail);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $order;
    }

    /**
     * Insert seller email in all order list
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();
        foreach ($orders as &$order) {
            $sellerEmail = $order->getSellerEmail();
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setSellerEmail($sellerEmail);
            $order->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

    /**
     * Insert seller email in order Get API
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(OrderRepositoryInterface $subject, $order)
    {
        $oldStatus = $order->getOrigData('status');
        $order->setSellerEmail(__("Buyed by Client"));
        if ($oldStatus === null) {
            $user = $this->authSession->getUser();
            if ($user) {
                $order->setSellerEmail($user->getEmail());
            }
        }
        $order->save();
        return $order;
    }
}
